module.exports = function (config) {
    config.set({
// Karma configuration

// base path, that will be used to resolve files and exclude
        basePath: '',

// list of files / patterns to load in the browser
        files: [
            'www/components/jquery/jquery.js',
            'www/components/angular/angular.js',
            'www/components/angular-mocks/angular-mocks.js',
            'www/components/angular-resource/angular-resource.js',
            'www/components/select2/select2.js',
            'www/components/underscore/underscore.js',
            'www/js/bootstrap-custom/ui-bootstrap-custom-tpls-0.5.0.min.js',
            'www/components/underscore.string/lib/underscore.string.js',
            'www/components/angular-ui/common/module.js',

            'www/js/admin/*.js',
            'www/js/frontend/*.js',
            'www/tests/admin/*.js',
            'www/tests/frontend/*.js'
        ],

// list of files to exclude
        exclude: [],

// test results reporter to use
// possible values: dots || progress || growl
        reporters: ['progress'],
        frameworks: ["jasmine"],

// web server port
        port: 8000,

// cli runner port
        runnerPort: 9100,

// enable / disable colors in the output (reporters and logs)
        colors: true,

// enable / disable watching file and executing tests whenever any file changes
        autoWatch: false,

// Start these browsers, currently available:
// - Chrome
// - ChromeCanary
// - Firefox
// - Opera
// - Safari (only Mac)
// - PhantomJS
// - IE (only Windows)
        browsers: ['PhantomJS'],

// If browser does not capture in given timeout [ms], kill it
        captureTimeout: 5000,

// Continuous Integration mode
// if true, it capture browsers, run tests and exit
        singleRun: true

    });
}
