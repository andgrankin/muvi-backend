'use strict';

angular.module('videorazbor', ['videorazbor.controllers', 'videorazbor.directives', 'videorazbor.filters']).
    config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
        $locationProvider.html5Mode(false);
        $routeProvider.when('/movie', {templateUrl: '/partials/admin/movies.html', controller: 'ListMoviesCtrl'});
        $routeProvider.when('/movie/add', {templateUrl: '/partials/admin/movies/add.html', controller: 'AddMovieCtrl'});
        $routeProvider.when('/movie/:id', {templateUrl: '/partials/admin/movies/add.html', controller: 'AddMovieCtrl'});

        $routeProvider.when('/property/:property', {templateUrl: '/partials/admin/movie-property.html', controller: 'PropertyCtrl'});


        $routeProvider.otherwise({redirectTo: '/'});
    }]).
    constant('movieProperty', {
        genre: {
            name: 'Жанры',
            route: '/admin/genre'
        },
        language: {
            name: 'Языки',
            route: '/admin/language'
        },
        instrument: {
            name: 'Инструменты',
            route: '/admin/instrument'
        },
        lesson_type: {
            name: 'Тип урока',
            route: '/admin/lesson_type'
        },
        technique: {
            name: 'Приемы игры',
            route: '/admin/technique'
        },
        music_property: {
            name: 'Музкальные свойства',
            route: '/admin/music_property'
        },
        tune: {
            name: 'Строй',
            route: '/admin/tune'
        },
        source: {
            name: 'Источник',
            route: '/admin/origin'
        }
    });