'use strict';

/* Controllers */

angular.module('videorazbor.controllers', ['ngResource', 'ui.select2', 'ui.bootstrap']).
    controller('ListMoviesCtrl', ['$scope', '$routeParams', '$location', '$resource', 'movieProperty', function ($scope, $routeParams, $location, $resource, movieProperty) {
        var Movie = $resource('/admin/movie/:allProperties/:id',
            {id: '@id'},
            {'query': {method: 'GET', isArray: false}}
        );

        $scope.currentPage = 1;

        getList();

        $scope.pageChanged = function(page) {
            getList(page);
        }

        $scope.deleteMovie = function (id) {
            if (confirm('Точно удалить?')) {
                Movie.delete({'id': id});
                    getList();
            }
        }

        $scope.showMultipleNames = function(items, delimeter){
            delimeter = delimeter || ', ';
            var names = [];
            if(!!items) {
                names =  _.reduce(items, function(memo, item){
                    memo.push(item.name)
                    return memo;
                }, []);
            }
            console.log(names)
            return names.join(delimeter)
        }

        function getList(page) {
            Movie.get({'page': page}, function(response){
                $scope.movies = response.result.movies;
                $scope.totalMovies = response.result.totalMovies;
                $scope.pageCount = response.result.pageCount;
            })
        }

    }]).constant('paginationConfig', {
        directionLinks: true,
        previousText: 'Предыдущая',
        nextText: 'Следующая',
        rotate: true
    })


    .controller('AddMovieCtrl', ['$timeout', '$scope', '$routeParams', '$location', '$resource', function ($timeout, $scope, $routeParams, $location, $resource) {
        $scope.properties = [];
        $scope.models = {};
        var Movie = $resource('/admin/movie/:allProperties/:id',
            {id: '@id'},
            {'query': {method: 'GET', isArray: false}}
        );

        Movie.get({allProperties: 'properties'}, function(response){
            $scope.properties = response.result;

            if($routeParams.id) {
                Movie.get({id: $routeParams.id}, function(response){
                    $scope.models = response.result;

                    _.each(['instrument', 'language', 'tune'], function(key){
                        if(!response.result[key]) {
                            return;
                        }
                        $scope.models[key] = _.find($scope.properties[key], function(p){
                            return p.id == response.result[key].id;
                        })
                    });

                    _.each(['lesson_type', 'technique', 'music_property', 'genre', 'origin'], function(key){
                        $scope.models[key] = getMultipleSelectValues($scope.properties, response.result, key);
                    });

                });
            }

        });


        $scope.saveMovie = function(){
            if(!$scope.addMovie.$valid ||
                !$scope.models.instrument) {
                alert('Выберите инструмент');
                return;
            }

            var movieData = {
                name: $scope.models.name,
                link: $scope.models.link,
                description: getValue($scope.models, 'description', false),
                tab: getValue($scope.models, 'tab', false),
                mediator: getValue($scope.models, 'mediator', false),
                capo: getValue($scope.models, 'capo', false),
                voice: getValue($scope.models, 'voice', false),
                genre: [],
                technique: [],
                music_property: [],
                origin: []
            };

            if(getValue($scope.models, 'id', false)) {
                movieData.id = getValue($scope.models, 'id', null);
            }

            if(getValue($scope.models, 'instrument', false)) {
                movieData.instrument = $scope.models['instrument'].id;
            }
            if(getValue($scope.models, 'language', false)) {
                movieData.language = $scope.models['language'].id;
            }
            if(getValue($scope.models, 'tune', false)) {
                movieData.tune = $scope.models['tune'].id;
            }

            var selects = ['genre', 'technique', 'music_property', 'origin', 'lesson_type'];

            $.each(selects, function(index, val){
                $.each(getValue($scope.models, val, []), function(index, element){
                    if (!movieData[val]) {
                        movieData[val] = [];
                    }
                    movieData[val].push(element.id);
                });
            });
            
            var m = new Movie(movieData);
            m.$save(function(a, b){
                $location.path('movie');
            });
        }

        function getValue(arr, key, deflt){
            deflt = deflt || false;

            if(!!arr && !!arr[key]) {
                return arr[key]
            }
            return deflt;
        }

        function getMultipleSelectValues(properties, savedProperties, variable){
            var out = [];
            _.each(properties[variable], function(property, index){
                _.each(savedProperties[variable], function(savedProperty){
                    if(property.id == savedProperty.id) {
                        out.push(property);
                    }
                });
            });
            return out;
        }

    }])

    .controller('PropertyCtrl', ['$scope', '$routeParams', '$location', '$resource', 'movieProperty', function ($scope, $routeParams, $location, $resource, movieProperty) {
        if (!movieProperty[$routeParams.property]) {
            $location.path('/')
        }

        var Property = $resource(movieProperty[$routeParams.property].route + '/:id',
            {id: '@id'},
            {'query': {method: 'GET', isArray: false}}
        );

        $scope.items = {};

        $scope.name = movieProperty[$routeParams.property].name;

        getList();

        $scope.save = function () {
            var p = new Property({
                id: $scope.item.id,
                unique_id: $scope.item.unique_id,
                name: $scope.item.name
            });
            p.$save();
            getList();
        }

        $scope.reset = function () {
            $scope.item = {};
        }

        $scope.editItem = function (id) {
            Property.get({'id': id}, function (response) {
                $scope.item = response.result;
            });
        }

        $scope.deleteItem = function (id) {
            if (confirm('Точно удалить?')) {
                Property.delete({'id': id});
                getList();
            }
        }

        $scope.updateItem = function (id) {
            $scope.item = Property.get({'id': id});
            //p.$save();
        }

        function getList() {
            Property.query(function (response) {
                $scope.items = response.result;
            });
        }


    }])
