"use strict";

angular.module('videorazbor.filters', []).filter('youtuber', [function () {
    /**
     * @var input string
     */
    return function (input) {
        var link = input.replace('youtube.com/embed/', 'youtube.com/watch?v=');
        if (link.indexOf('http://') === -1 && link.indexOf('https://') === -1) {
            link = 'http://' + link;
        }
        return link;
    }
}]);