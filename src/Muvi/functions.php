<?php

namespace Muvi\Functions;

use Muvi\Model\ModelAbstract;
use Muvi\Model\Movie;
use Muvi\Storage\MoviePropertyStorage;
use Symfony\Component\HttpFoundation\Request;


/**
 * @param $models ModelAbstract[]
 * @return []
 */
function models_to_arrays($models){
    $out = [];
    foreach ($models as $m) {
        $out[] = $m->toArray();
    }
    return $out;

}

function get_youtube_id($url){
    $parts = explode('/', $url);
    return end($parts);
}


function get_page_count($totalResults, $moviesOnPage)
{
    $pageCount = intval($totalResults / $moviesOnPage);
    if($pageCount > 0) {
        $pageCount += $totalResults % $moviesOnPage? 1: 0;
    }
    return $pageCount;
}


/**
 * @param Request $request
 * @param $db
 * @return Movie
 */
function createMovieFromRequest(Request $request, $db) {

    $data = [];

    $data = [
        'name' => $request->request->get('name'),
        'link' => $request->request->get('link'),
        'description' => $request->request->get('description'),
        'tab' => $request->request->get('tab', false),
        'mediator' => $request->request->get('mediator', false),
        'capo' => $request->request->get('capo', false),
        'voice' => $request->request->get('voice', false)
    ];

    if($request->request->get('id')) {
        $data['id'] = $request->request->get('id');
    }


    $selectBoxes = ['genre', 'technique', 'music_property', 'origin', 'lesson_type'];


    if($request->request->get('language')) {
        $data['language'] = (new MoviePropertyStorage($db, 'language'))->getById($request->request->get('language'));
    }

    if($request->request->get('instrument')) {
        $data['instrument'] = (new MoviePropertyStorage($db, 'instrument'))->getById($request->request->get('instrument'));
    }
    if($request->request->get('tune')) {
        $data['tune'] = (new MoviePropertyStorage($db, 'tune'))->getById($request->request->get('tune'));
    }


    foreach ($selectBoxes as $propertyName) {
        $propertyStorage = new MoviePropertyStorage($db, $propertyName);
        $ids = $request->request->get($propertyName, []);

        foreach ($ids as $id) {
            $data[$propertyName][] = $propertyStorage->getById($id);
        }
    }
    return new Movie($data);

}