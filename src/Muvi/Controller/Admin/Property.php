<?php

namespace Muvi\Controller\Admin;

use Muvi\Model\Movie;
use Muvi\Model\SimpleModel;
use Muvi\Storage\MoviePropertyStorage;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Functional as F;

class Property {

    public function allProperties(Application $app){
        $out = [
            'result' => [],
            'error' => [],
        ];

        foreach ($app['availableProperties'] as $p) {
            $s = new MoviePropertyStorage($app['db'], $p);
            $models = $s->getList([
                'offset' => 0,
                'limit' => 10000 // need all
            ]);

            if(count($models)){
                foreach ($models as $m) {
                    $out['result'][$p][] = $m->toArray();
                }
            }
            else{
                $out['result'][$p] = [];
            }
        }

        return $app->json($out);
    }

    public function propertyList(Application $app, $property){
        if(!in_array($property, $app['availableProperties'])) {
            return 'Do not have specified property';
        }

        $s = new MoviePropertyStorage($app['db'], $property);

        $models = $s->getList([
            'offset' => 0,
            'limit' => 10000
        ]);
        $out = [
            'result' => [],
            'error' => [],
        ];

        foreach ($models as $m) {
            $out['result'][] = $m->toArray();
        }
        return $app->json($out);
    }

    public function getProperty(Application $app, $property, $id){
        $s = new MoviePropertyStorage($app['db'], $property);
        if(!in_array($property, $app['availableProperties'])) {
            return 'Do not have specified property';
        }

        $model = $s->getById($id);
        $out = [
            'result' => $model->toArray(),
            'error' => [],
        ];

        return $app->json($out);
    }

    public function saveProperty(Application $app, Request $request, $property){
        if(!in_array($property, $app['availableProperties'])) {
            return 'Do not have specified property';
        }

        $propertyStorage = new MoviePropertyStorage($app['db'], $property);

        $propertyStorage->save(new SimpleModel('genre', [
            'unique_id' => $request->get('unique_id'),
            'name' => $request->get('name'),
            'id' => $request->get('id', null)
        ]));
        return '';
    }

    public function deleteProperty(Application $app, $id, $property){
        $propertyStorage = new MoviePropertyStorage($app['db'], $property);
        $deleted = $propertyStorage->delete($id);
        return $app->json(['result' => "$deleted deleted"]);
    }

    /**
     * @param $s
     * @param $moviesOnPage
     * @param $out
     * @return mixed
     */
    public function getPageCount($totalResults, $moviesOnPage)
    {
        $pageCount = intval($totalResults / $moviesOnPage);
        if($pageCount > 0) {
            $pageCount += $totalResults % $moviesOnPage;
        }
        return $pageCount;
    }

}