<?php

namespace Muvi\Controller\Admin;

use Muvi\Model\SimpleModel;
use Muvi\Storage\MoviePropertyStorage;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class Movie {

    private $avaliableProperties = [
        'language',
        'tune',
        'origin',
        'genre',
        'lesson_type',
        'music_property',
        'technique',
        'instrument'
    ];

    public function index(Application $app, Request $request){
        return $app['twig']->render('admin/index.html.twig', array(
            'title'         => 'ЦУП',
            'error'         => $app['security.last_error']($request),
            'last_username' => $app['session']->get('_security.last_username'),
        ));
    }

    public function movies(Application $app, Request $request){
        $out = [
            'result' => [],
            'error' => [],
        ];
        $s = new \Muvi\Storage\Movie($app['db']);
        $page = $request->get('page')=='all'? 'all': intval($request->get('page', 1));
        if($page === 'all'){
            $models = $s->getAll(['order_by' => [
                    [
                        'sort' => 'created',
                        'order' => 'DESC'
                    ],
                    [
                        'sort' => 'id',
                        'order' => 'DESC'
                    ]
            ]]);
            $moviesOnPage = $s->getCount();
        }
        else{
            $page = $page < 1 ?1: $page;
            $moviesOnPage = !$app['admin.main.controller.movies_on_page']? 30: $app['admin.main.controller.movies_on_page'];
            $offset = ($page -1)*$moviesOnPage;
            $models = $s->getList(['limit' => $moviesOnPage, 'offset' => $offset  ,'order_by' => [
                    [
                        'sort' => 'created',
                        'order' => 'DESC'
                    ],
                    [
                        'sort' => 'id',
                        'order' => 'DESC'
                    ]
                ]]);
        }

        $out['result']['pageCount'] = $this->getPageCount($s->getCount(), $moviesOnPage);
        $out['result']['totalMovies'] = $s->getCount();

        /** @var  Movie [] $models */
        if(count($models)){
            foreach ($models as $m) {
                $out['result']['movies'][] = $m->toArray();
            }
        }
        else{
            $out['result']['movies'][] = [];
        }

        return $app->json($out);
    }

    public function getMovie(Application $app, $id){

        $s = new \Muvi\Storage\Movie($app['db']);
        $movie = $s->getById($id);

        return $app->json([
                'result' => $movie->toArray(),
                'error' => []
        ]);

    }

    public function saveMovie(Application $app, Request $request){
        $s = new \Muvi\Storage\Movie($app['db']);
        $s->save(\Muvi\Functions\createMovieFromRequest($request, $app['db']));
        return '';
    }

    public function deleteMovie(Application $app, $id){
        $s = new \Muvi\Storage\Movie($app['db']);
        $s->delete($id);
        return '';
    }
    

    /**
     * @param $s
     * @param $moviesOnPage
     * @param $out
     * @return mixed
     */
    public function getPageCount($totalResults, $moviesOnPage)
    {
        $pageCount = intval($totalResults / $moviesOnPage);
        if($pageCount > 0) {
            $pageCount += $totalResults % $moviesOnPage;
        }
        return $pageCount;
    }

}