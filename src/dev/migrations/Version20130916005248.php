<?php

namespace MuVi\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20130916005248 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("CREATE TABLE movie_views (
            id serial NOT NULL,
            movie_id integer NOT NULL,
            user_agent character varying(255),
            ip inet NOT NULL,
            time timestamp without time zone DEFAULT now() NOT NULL,
            CONSTRAINT pk_movies_views PRIMARY KEY (id),
            CONSTRAINT fk_so_movies_views_ref_movie_id FOREIGN KEY (movie_id) REFERENCES movie(id) ON UPDATE CASCADE ON DELETE CASCADE
        )");


    }

    public function down(Schema $schema)
    {
        $this->addSql('DROP TABLE movie_views');
    }
}
