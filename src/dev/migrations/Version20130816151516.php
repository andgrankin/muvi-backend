<?php

namespace MuVi\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20130816151516 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql('ALTER TABLE movie ADD created TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT now()');
    }

    public function down(Schema $schema)
    {
        $table = $schema->getTable('movie');
        $table->dropColumn('created');

    }
}
