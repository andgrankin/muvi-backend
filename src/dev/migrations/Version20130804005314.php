<?php

namespace MuVi\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20130804005314 extends AbstractMigration
{

    private $tables = [
        'language',
        'tune',
        'origin',
        'genre',
        'lesson_type',
        'music_property',
        'technique',
        'instrument',
    ];

    public function up(Schema $schema)
    {
        foreach ($this->tables as $t) {
            $table = $schema->createTable($t);
            $table->addColumn('id', 'integer', [
                'notnull' => true,
                'autoincrement' => true
            ]);
            $table->addColumn('unique_id', 'string', [
                'notnull' => true,
                'length' => 100
            ]);
            $table->addColumn('name', 'string', [
                'notnull' => true,
                'length' => 100
            ]);
            $table->setPrimaryKey(['id']);
        }

        $table = $schema->createTable('movie');
        $table->addColumn('id', 'integer', [
            'notnull' => true,
            'autoincrement' => true
        ]);
        $table->addColumn('name', 'string', [
            'notnull' => true,
            'length' => 255
        ]);
        $table->addColumn('link', 'string', [
            'notnull' => true,
            'length' => 255
        ]);

        $table->addColumn('language', 'integer', [
            'notnull' => true,
        ]);
        $table->addColumn('tune', 'integer');
        $table->addColumn('instrument', 'integer', [
            'notnull' => true,
        ]);
        $table->addColumn('tab', 'boolean', [
            'notnull' => true,
        ]);
        $table->addColumn('capo', 'boolean', [
            'notnull' => true,
        ]);
        $table->addColumn('mediator', 'boolean', [
            'notnull' => true,
        ]);
        $table->addColumn('voice', 'boolean', [
            'notnull' => true,
        ]);
        $table->addColumn('description', 'text');
        $table->setPrimaryKey(['id']);


        $table = $schema->createTable('movie_origin');
        $table->addColumn('id', 'integer', [
            'notnull' => true,
            'autoincrement' => true,
            'primary' => true
        ]);
        $table->addColumn('movie_id', 'integer', [
            'notnull' => true
        ]);
        $table->addColumn('origin_id', 'integer', [
            'notnull' => true
        ]);
        $table->addForeignKeyConstraint('movie', ['movie_id'], ['id'],
            [
                'onUpdate' => 'CASCADE',
                'onDelete' => 'CASCADE'
            ]);
        $table->addForeignKeyConstraint('origin', ['origin_id'], ['id'],
            [
                'onUpdate' => 'CASCADE',
                'onDelete' => 'CASCADE'
            ]);



        $table = $schema->createTable('movie_lesson_type');
        $table->addColumn('id', 'integer', [
            'notnull' => true,
            'autoincrement' => true,
            'primary' => true
        ]);
        $table->addColumn('movie_id', 'integer', [
            'notnull' => true
        ]);
        $table->addColumn('lesson_type_id', 'integer', [
            'notnull' => true
        ]);
        $table->addForeignKeyConstraint('movie', ['movie_id'], ['id'],
            [
                'onUpdate' => 'CASCADE',
                'onDelete' => 'CASCADE'
            ]);
        $table->addForeignKeyConstraint('lesson_type', ['lesson_type_id'], ['id'],
            [
                'onUpdate' => 'CASCADE',
                'onDelete' => 'CASCADE'
            ]);


        $table = $schema->createTable('movie_genre');
        $table->addColumn('id', 'integer', [
            'notnull' => true,
            'autoincrement' => true,
            'primary' => true
        ]);
        $table->addColumn('movie_id', 'integer', [
            'notnull' => true
        ]);
        $table->addColumn('genre_id', 'integer', [
            'notnull' => true
        ]);
        $table->addForeignKeyConstraint('movie', ['movie_id'], ['id'],
            [
                'onUpdate' => 'CASCADE',
                'onDelete' => 'CASCADE'
            ]);
        $table->addForeignKeyConstraint('genre', ['genre_id'], ['id'],
            [
                'onUpdate' => 'CASCADE',
                'onDelete' => 'CASCADE'
            ]);


        $table = $schema->createTable('movie_music_property');
        $table->addColumn('id', 'integer', [
            'notnull' => true,
            'autoincrement' => true,
            'primary' => true
        ]);
        $table->addColumn('movie_id', 'integer', [
            'notnull' => true
        ]);
        $table->addColumn('music_property_id', 'integer', [
            'notnull' => true
        ]);
        $table->addForeignKeyConstraint('movie', ['movie_id'], ['id'],
            [
                'onUpdate' => 'CASCADE',
                'onDelete' => 'CASCADE'
            ]);
        $table->addForeignKeyConstraint('music_property', ['music_property_id'], ['id'],
            [
                'onUpdate' => 'CASCADE',
                'onDelete' => 'CASCADE'
            ]);


        $table = $schema->createTable('movie_technique');
        $table->addColumn('id', 'integer', [
            'notnull' => true,
            'autoincrement' => true,
            'primary' => true
        ]);
        $table->addColumn('movie_id', 'integer', [
            'notnull' => true
        ]);
        $table->addColumn('technique_id', 'integer', [
            'notnull' => true
        ]);
        $table->addForeignKeyConstraint('movie', ['movie_id'], ['id'],
            [
                'onUpdate' => 'CASCADE',
                'onDelete' => 'CASCADE'
            ]);
        $table->addForeignKeyConstraint('technique', ['technique_id'], ['id'],
            [
                'onUpdate' => 'CASCADE',
                'onDelete' => 'CASCADE'
            ]);
    }

    public function down(Schema $schema)
    {
        foreach ($this->tables as $t) {
            $schema->dropTable($t);
        }

        $schema->dropTable('movie');
        $schema->dropTable('movie_origin');

        $schema->dropTable('movie_lesson_type');
        $schema->dropTable('movie_genre');
        $schema->dropTable('movie_music_property');
        $schema->dropTable('movie_technique');
        $schema->dropTable('technique');


    }
}
