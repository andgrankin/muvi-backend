<?php
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app = new Silex\Application();

$env = getenv('APP_ENV') ?: 'prod';

require_once __DIR__."/../config/config.php";
require_once __DIR__."/../config/{$env}.php";

$app->register(new Silex\Provider\ServiceControllerServiceProvider());
$app->register(new Silex\Provider\SessionServiceProvider());
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
$app->register(new Silex\Provider\FormServiceProvider());

$app->register(new Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__.'/../logs/development.log',
));

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => $config['db'],
));

if ( $app['debug'] ) {
    $logger = new Doctrine\DBAL\Logging\DebugStack();
    $app['db.config']->setSQLLogger($logger);
    $app->error(function(\Exception $e, $code) use ($app, $logger) {
        if ( $e instanceof PDOException and count($logger->queries) ) {
            // We want to log the query as an ERROR for PDO exceptions!
            $query = array_pop($logger->queries);
            $app['monolog']->err($query['sql'], array(
                'params' => $query['params'],
                'types' => $query['types']
            ));
        }
    });
    $app->after(function(Request $request, Response $response) use ($app, $logger) {
        // Log all queries as DEBUG.
        foreach ( $logger->queries as $query ) {
            $app['monolog']->debug($query['sql'], array(
                'params' => $query['params'],
                'types' => $query['types']
            ));
        }
    });
}


$app->register(new Silex\Provider\SecurityServiceProvider(), array(
    'security.firewalls' => array(
        'admin' => array(
            'pattern' => '^/admin',
            'form'    => array(
                'login_path' => '/login',
                'check_path' => '/admin/login_check',
            ),
            'users' => array(
                'admin' => array('ROLE_ADMIN', 'nissan476'),
            ),
        ),
    ),
));

// error
$app->error(
    function (\Exception $e, $code) use ($app) {
        if ($e instanceof \Symfony\Component\HttpKernel\Exception\HttpException) {
            // if we got http exception, then message was already predefined.
            $message = $e->getMessage();
        } else {
            switch ($code) {
                case 404:
                    $message = 'Page not found.';
                    break;
                default:
                    $message = $code . ': ALL BROKEN!111!' . $e->getMessage();
            }
        }

        if ($app['request']->isXmlHttpRequest()) {
            return new Response($message, $code);
        } else {
            // TODO: add design
            return new Response($message, $code);
        }
    }
);



$app->before(function (Request $request) use ($app) {
    $path = $app['maintenance_page'];

    if (false !== $path && // move backend to separate app and reimplement that
        is_readable($path) &&
        strpos($request->getPathInfo(), '/admin') !== 0 &&
        $request->getPathInfo() !== '/login'
    ) {
        return new Response(file_get_contents($path), 503);
    }


    if (0 === strpos($request->headers->get('Accept'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
});


$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../views/',
    'twig.options'        => array(
        'cache'            => isset($app['twig.options.cache']) ? $app['twig.options.cache'] : false,
        'strict_variables' => true
    ),
    'twig.form.templates' => array('form_div_layout.html.twig', 'common/form_div_layout.html.twig'),
));
