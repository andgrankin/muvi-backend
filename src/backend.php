<?php

$backend = $app['controllers_factory'];

$app['admin.movies.controller'] = $app->share(function() {
    return new \Muvi\Controller\Admin\Movie();
});

$app['admin.properties.controller'] = $app->share(function() {
    return new \Muvi\Controller\Admin\Property();
});


$backend->get('', 'admin.movies.controller:index');
$backend->get('/movie', 'admin.movies.controller:movies');
$backend->post('/movie', 'admin.movies.controller:saveMovie');

$backend->get('/movie/{id}', 'admin.movies.controller:getMovie')->assert('id', '\d+');
$backend->post('/movie/{id}', 'admin.movies.controller:saveMovie')->assert('id', '\d+');
$backend->delete('/movie/{id}', 'admin.movies.controller:deleteMovie')->assert('id', '\d+');


$backend->get('/movie/properties', 'admin.properties.controller:allProperties');

$backend->get('/{property}', 'admin.properties.controller:propertyList')->assert('property', '\w+');;
$backend->get('/{property}/{id}', 'admin.properties.controller:getProperty')->assert('id', '\d+')
    ->assert('property', '\w+');;
$backend->post('/{property}', 'admin.properties.controller:saveProperty')
    ->assert('property', '\w+');;
$backend->post('/{property}/{id}', 'admin.properties.controller:saveProperty')
    ->assert('id', '\d+')
    ->assert('property', '\w+');;
$backend->delete('/{property}/{id}', 'admin.properties.controller:deleteProperty')
    ->assert('id', '\d+')
    ->assert('property', '\w+');;


return $backend;
