<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

require_once __DIR__ . '/../vendor/autoload.php';

require_once __DIR__ . '/bootstrap.php';

/** $app  new Silex\Application(); */


$app['security.encoder.digest'] = $app->share(
    function () {
        return new \Symfony\Component\Security\Core\Encoder\PlaintextPasswordEncoder();
    }
);


$app->get(
    '/',
    function () use ($app) {
        return $app->redirect('/admin/');
    }
);


$app->get(
    '/login',
    function (Request $request) use ($app) {
        return $app['twig']->render(
            'login.html.twig',
            array(
                'error' => $app['security.last_error']($request),
                'last_username' => $app['session']->get('_security.last_username'),
            )
        );
    }
);


$app->mount('/admin', require_once __DIR__ . '/backend.php');